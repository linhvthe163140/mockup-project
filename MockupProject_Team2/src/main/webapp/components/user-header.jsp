<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<div id="header-wrap">
	<div class="top-content">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="social-links"></div>
					<!--social-links-->
				</div>
				<div class="col-md-6">
					<div class="right-element">
						<div class="action-menu">
							<div class="search-bar">
								<a href="#" class="search-button search-toggle"
									data-selector="#header-wrap"> <i class="icon icon-search"></i>
								</a>
								<form role="search" method="get" class="search-box">
									<input class="search-field text search-input"
										placeholder="Search" type="search">
								</form>
							</div>
						</div>
						<a href="#" class="user-account for-buy"><i
							class="icon icon-user"></i><span>Account</span></a> <a href="#"
							class="user-account for-buy"></i><span>Logout</span></a>
					</div>
					<!--top-right-->
				</div>
			</div>
		</div>
	</div>
	<!--top-content-->

	<header id="header">
		<div class="container">
			<div class="row">

				<div class="col-md-2">
					<div class="main-logo">
						<a href="index.html"><img src="images/main-logo.png"
							alt="logo"></a>
					</div>

				</div>

				<div class="col-md-10">

					<nav id="navbar">
						<div class="main-menu stellarnav">
							<ul class="menu-list">
								<li class="menu-item active"><a href="index.html"
									data-effect="Home">Home</a></li>
								<li class="menu-item has-sub"><a href="#pages"
									class="nav-link" data-effect="Pages">Pages</a>
									<ul>
										<li class="active"><a href="index.html">Home</a></li>
										<li><a href="listBook.html">Book List</a></li>
										<li><a href="bookcase.html">Book Case</a></li>
										<li><a href="contact.html">Contact</a></li>
									</ul></li>
								<li class="menu-item"><a href="listBook.html"
									class="nav-link" data-effect="Articles">Book List</a></li>
								<li class="menu-item"><a href="bookcase.html"
									class="nav-link" data-effect="Bookcase">My Bookcase</a></li>
								<li class="menu-item"><a href="contact.html"
									class="nav-link" data-effect="Contact">Contact</a></li>
							</ul>

							<div class="hamburger">
								<span class="bar"></span> <span class="bar"></span> <span
									class="bar"></span>
							</div>

						</div>
					</nav>

				</div>

			</div>
		</div>
	</header>

</div>
<!--header-wrap-->

