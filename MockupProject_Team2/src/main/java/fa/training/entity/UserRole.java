package fa.training.entity;

public class UserRole {
	private int roleID;
    private String roleName;
    
    public UserRole() {
		// TODO Auto-generated constructor stub
	}

	public UserRole(int roleID, String roleName) {
		super();
		this.roleID = roleID;
		this.roleName = roleName;
	}

	public int getRoleID() {
		return roleID;
	}

	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Override
	public String toString() {
		return "UserRole [roleID=" + roleID + ", roleName=" + roleName + "]";
	}
}
