<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">
<head>
<meta charset="utf-8" />
<meta name="author" content="Themezhub" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SkillUp - Online Learning Platform</title>

<!-- Custom CSS -->
<link href="assets/css/styles.css" rel="stylesheet">

</head>

<body>

	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pages.scss -->
	<!-- ============================================================== -->
	<div id="main-wrapper">
		<div class="header header-light">
			<div class="container">
				<%@include file="components/admin-header.jsp"%>
			</div>
		</div>

		<section class="gray pt-4">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-3 col-md-3">
						<%@include file="components/admin-sidebar.jsp"%>
					</div>
				</div>

				<div class="col-lg-9 col-md-9 col-sm-12">

					<!-- Row -->
					<div class="row justify-content-between">
						<div class="col-lg-12 col-md-12 col-sm-12 pb-4">
							<div
								class="dashboard_wrap d-flex align-items-center justify-content-between">
								<div class="arion">
									<nav class="transparent">
										<ol class="breadcrumb">
											<li class="breadcrumb-item"><a href="#">Home</a></li>
											<li class="breadcrumb-item active" aria-current="page">Manage
												Courses</li>
										</ol>
									</nav>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->

					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12">
							<div class="dashboard_wrap">

								<div class="row">
									<div class="col-xl-12 col-lg-12 col-md-12 mb-4">
										<h6 class="m-0">All Courses List</h6>
									</div>
								</div>

								<div class="row align-items-end mb-5">
									<div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
										<div class="form-group">
											<label>Categories</label>
											<div class="smalls">
												<select id="cates" class="form-control">
													<option value="">&nbsp;</option>
													<option value="1">IT & Software</option>
													<option value="2">Banking</option>
													<option value="3">Medical</option>
													<option value="4">Insurence</option>
													<option value="5">Finance & Accounting</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
										<div class="form-group">
											<label>Instructor</label>
											<div class="smalls">
												<select id="ins" class="form-control">
													<option value="">&nbsp;</option>
													<option value="1">Summer D. Friedel</option>
													<option value="2">Daniel D. Richards</option>
													<option value="3">Rosemary K. Delisle</option>
													<option value="4">Joseph S. Whetstone</option>
													<option value="5">Roger M. Gragg</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xl-2 col-lg-4 col-md-6 col-sm-6">
										<div class="form-group">
											<label>Status</label>
											<div class="smalls">
												<select id="sts" class="form-control">
													<option value="">&nbsp;</option>
													<option value="1">Active</option>
													<option value="2">Incoming</option>
													<option value="3">Expired</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xl-2 col-lg-4 col-md-6 col-sm-6">
										<div class="form-group">
											<label>Price</label>
											<div class="smalls">
												<select id="prc" class="form-control">
													<option value="">&nbsp;</option>
													<option value="1">All</option>
													<option value="2">Free</option>
													<option value="3">Paid</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xl-2 col-lg-4 col-md-6 col-sm-6">
										<div class="form-group">
											<button type="button"
												class="btn text-white full-width theme-bg">Filter</button>
										</div>
									</div>
								</div>

								<div class="row justify-content-between">
									<div class="col-xl-2 col-lg-3 col-md-6">
										<div class="form-group smalls row align-items-center">
											<label class="col-xl-3 col-lg-3 col-sm-2 col-form-label">Show</label>
											<div class="col-xl-9 col-lg-9 col-sm-10">
												<select id="show" class="form-control">
													<option value="">&nbsp;</option>
													<option value="1">10</option>
													<option value="2">25</option>
													<option value="3">35</option>
													<option value="3">50</option>
													<option value="3">100</option>
													<option value="3">250</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xl-4 col-lg-5 col-md-6">
										<div class="form-group smalls row align-items-center">
											<label class="col-xl-2 col-lg-2 col-sm-2 col-form-label">Search</label>
											<div class="col-xl-10 col-lg-10 col-sm-10">
												<input type="text" class="form-control">
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-xl-12 col-lg-12 col-md-12 mb-2">
										<div class="table-responsive">
											<table class="table dash_list">
												<thead>
													<tr>
														<th scope="col">#</th>
														<th scope="col">Title</th>
														<th scope="col">Category</th>
														<th scope="col">Lectures</th>
														<th scope="col">Enrolled</th>
														<th scope="col">Status</th>
														<th scope="col">Price</th>
														<th scope="col">Action</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<th scope="row">1</th>
														<td><h6>UI/UX Design Pattern For Succesfull
																Software Applications</h6>
															<p>
																Instructor:<span>Gabriel L. William</span>
															</p></td>
														<td><div class="dhs_tags">Finance</div></td>
														<td><div class="smalls">17 Lectures</div></td>
														<td><span class="smalls">Total: 12</span></td>
														<td><span class="trip theme-cl theme-bg-light">Active</span></td>
														<td><span class="trip theme-cl theme-bg-light">Free</span></td>
														<td>
															<div class="dropdown show">
																<a class="btn btn-action" href="#"
																	data-toggle="dropdown" aria-haspopup="true"
																	aria-expanded="false"> <i class="fas fa-ellipsis-h"></i>
																</a>
																<div class="drp-select dropdown-menu">
																	<a class="dropdown-item" href="JavaScript:Void(0);">View</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Edit</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Delete</a>
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">2</th>
														<td><h6>Basic Knowledge About Hodiernal Bharat
																In History</h6>
															<p>
																Instructor:<span>Theresa P. Crane</span>
															</p></td>
														<td><div class="dhs_tags">Laravel</div></td>
														<td><div class="smalls">22 Lectures</div></td>
														<td><span class="smalls">Total: 2</span></td>
														<td><span class="trip text-danger bg-light-danger">Expired</span></td>
														<td><span class="trip theme-cl theme-bg-light">Free</span></td>
														<td>
															<div class="dropdown show">
																<a class="btn btn-action" href="#"
																	data-toggle="dropdown" aria-haspopup="true"
																	aria-expanded="false"> <i class="fas fa-ellipsis-h"></i>
																</a>
																<div class="drp-select dropdown-menu">
																	<a class="dropdown-item" href="JavaScript:Void(0);">View</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Edit</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Delete</a>
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">3</th>
														<td><h6>Advance PHP Knowledge With Laravel To
																Make Smart Web</h6>
															<p>
																Instructor:<span>David Dhavan</span>
															</p></td>
														<td><div class="dhs_tags">Software</div></td>
														<td><span class="smalls">41 Lectures</span></td>
														<td><span class="smalls">Total: 4</span></td>
														<td><span class="trip theme-cl theme-bg-light">Active</span></td>
														<td><span class="trip gray">$99</span></td>
														<td>
															<div class="dropdown show">
																<a class="btn btn-action" href="#"
																	data-toggle="dropdown" aria-haspopup="true"
																	aria-expanded="false"> <i class="fas fa-ellipsis-h"></i>
																</a>
																<div class="drp-select dropdown-menu">
																	<a class="dropdown-item" href="JavaScript:Void(0);">View</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Edit</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Delete</a>
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">4</th>
														<td><h6>The Complete Accounting & Bank Financial
																Course 2021</h6>
															<p>
																Instructor:<span>Stephen E. Foss</span>
															</p></td>
														<td><div class="dhs_tags">Magento</div></td>
														<td><span class="smalls">56 Lectures</span></td>
														<td><span class="smalls">Total: 8</span></td>
														<td><span class="trip theme-cl theme-bg-light">Active</span></td>
														<td><span class="trip theme-cl theme-bg-light">Free</span></td>
														<td>
															<div class="dropdown show">
																<a class="btn btn-action" href="#"
																	data-toggle="dropdown" aria-haspopup="true"
																	aria-expanded="false"> <i class="fas fa-ellipsis-h"></i>
																</a>
																<div class="drp-select dropdown-menu">
																	<a class="dropdown-item" href="JavaScript:Void(0);">View</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Edit</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Delete</a>
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">5</th>
														<td><h6>The Complete Business Plan Course
																Includes 50 Templates</h6>
															<p>
																Instructor:<span>Jeannette B. Curiel</span>
															</p></td>
														<td><div class="dhs_tags">Business</div></td>
														<td><span class="smalls">40 Lectures</span></td>
														<td><span class="smalls">Total: 9</span></td>
														<td><span class="trip text-danger bg-light-danger">Expired</span></td>
														<td><span class="trip gray">$100</span></td>
														<td>
															<div class="dropdown show">
																<a class="btn btn-action" href="#"
																	data-toggle="dropdown" aria-haspopup="true"
																	aria-expanded="false"> <i class="fas fa-ellipsis-h"></i>
																</a>
																<div class="drp-select dropdown-menu">
																	<a class="dropdown-item" href="JavaScript:Void(0);">View</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Edit</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Delete</a>
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">6</th>
														<td><h6>Full Web Designing Course With 20 Web
																Template Designing</h6>
															<p>
																Instructor:<span>Daniel E. Theriault</span>
															</p></td>
														<td><div class="dhs_tags">Science</div></td>
														<td><span class="smalls">22 Lectures</span></td>
														<td><span class="smalls">Total: 0</span></td>
														<td><span class="trip theme-cl theme-bg-light">Active</span></td>
														<td><span class="trip theme-cl theme-bg-light">Free</span></td>
														<td>
															<div class="dropdown show">
																<a class="btn btn-action" href="#"
																	data-toggle="dropdown" aria-haspopup="true"
																	aria-expanded="false"> <i class="fas fa-ellipsis-h"></i>
																</a>
																<div class="drp-select dropdown-menu">
																	<a class="dropdown-item" href="JavaScript:Void(0);">View</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Edit</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Delete</a>
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">7</th>
														<td><h6>Full Web Hosting Course With 20 Web
																Template Designing</h6>
															</h6>
															<p>
																Instructor:<span>Brian B. Cohn</span>
															</p></td>
														<td><div class="dhs_tags">JavaScript</div></td>
														<td><span class="smalls">32 Lectures</span></td>
														<td><span class="smalls">Total: 8</span></td>
														<td><span class="trip theme-cl theme-bg-light">Active</span></td>
														<td><span class="trip gray">$49</span></td>
														<td>
															<div class="dropdown show">
																<a class="btn btn-action" href="#"
																	data-toggle="dropdown" aria-haspopup="true"
																	aria-expanded="false"> <i class="fas fa-ellipsis-h"></i>
																</a>
																<div class="drp-select dropdown-menu">
																	<a class="dropdown-item" href="JavaScript:Void(0);">View</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Edit</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Delete</a>
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">8</th>
														<td><h6>Advance Laravel Coding Course Advance
																Technology</h6>
															<p>
																Instructor:<span>Joshua G. Godinez</span>
															</p></td>
														<td><div class="dhs_tags">PHP</div></td>
														<td><span class="smalls">20 Lectures</span></td>
														<td><span class="smalls">Total: 7</span></td>
														<td><span class="trip text-danger bg-light-danger">Expired</span></td>
														<td><span class="trip theme-cl theme-bg-light">Free</span></td>
														<td>
															<div class="dropdown show">
																<a class="btn btn-action" href="#"
																	data-toggle="dropdown" aria-haspopup="true"
																	aria-expanded="false"> <i class="fas fa-ellipsis-h"></i>
																</a>
																<div class="drp-select dropdown-menu">
																	<a class="dropdown-item" href="JavaScript:Void(0);">View</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Edit</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Delete</a>
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<th scope="row">9</th>
														<td><h6>Full Web Designing Course With 20 Web
																Template Designing</h6>
															<p>
																Instructor:<span>R. Lindsley</span>
															</p></td>
														<td><div class="dhs_tags">WordPress</div></td>
														<td><span class="smalls">10 Lectures</span></td>
														<td><span class="smalls">Total: 6</span></td>
														<td><span class="trip theme-cl theme-bg-light">Active</span></td>
														<td><span class="trip theme-cl theme-bg-light">Free</span></td>
														<td>
															<div class="dropdown show">
																<a class="btn btn-action" href="#"
																	data-toggle="dropdown" aria-haspopup="true"
																	aria-expanded="false"> <i class="fas fa-ellipsis-h"></i>
																</a>
																<div class="drp-select dropdown-menu">
																	<a class="dropdown-item" href="JavaScript:Void(0);">View</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Edit</a>
																	<a class="dropdown-item" href="JavaScript:Void(0);">Delete</a>
																</div>
															</div>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>

								<div class="row align-items-center justify-content-between">
									<div class="col-xl-6 col-lg-6 col-md-12">
										<p class="p-0">Showing 1 to 15 of 15 entire</p>
									</div>
									<div class="col-xl-6 col-lg-6 col-md-12">
										<nav class="float-right">
											<ul class="pagination smalls m-0">
												<li class="page-item disabled"><a class="page-link"
													href="#" tabindex="-1"><i
														class="fas fa-arrow-circle-left"></i></a></li>
												<li class="page-item"><a class="page-link" href="#">1</a></li>
												<li class="page-item active"><a class="page-link"
													href="#">2 <span class="sr-only">(current)</span></a></li>
												<li class="page-item"><a class="page-link" href="#">3</a></li>
												<li class="page-item"><a class="page-link" href="#"><i
														class="fas fa-arrow-circle-right"></i></a></li>
											</ul>
										</nav>
									</div>
								</div>

							</div>
						</div>
					</div>


				</div>

			</div>
	</div>
	</section>
	<!-- ============================ Dashboard: Dashboard End ================================== -->

	<!-- ============================ Call To Action ================================== -->
	<section class="theme-bg call_action_wrap-wrap">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">

					<div class="call_action_wrap">
						<div class="call_action_wrap-head">
							<h3>Do You Have Questions ?</h3>
							<span>We'll help you to grow your career and growth.</span>
						</div>
						<a href="#" class="btn btn-call_action_wrap">Contact Us Today</a>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- ============================ Call To Action End ================================== -->

	<!-- ============================ Footer Start ================================== -->
	<footer class="dark-footer skin-dark-footer style-2">
		<div class="footer-middle">
			<div class="container">
				<div class="row">

					<div class="col-lg-5 col-md-5">
						<div class="footer_widget">
							<img src="assets/img/logo-light.png"
								class="img-footer small mb-2" alt="" />
							<h4 class="extream mb-3">
								Do you need help with<br>anything?
							</h4>
							<p>Receive updates, hot deals, tutorials, discounts sent
								straignt in your inbox every month</p>
							<div class="foot-news-last">
								<div class="input-group">
									<input type="text" class="form-control"
										placeholder="Email Address">
									<div class="input-group-append">
										<button type="button"
											class="input-group-text theme-bg b-0 text-light">Subscribe</button>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-lg-6 col-md-7 ml-auto">
						<div class="row">

							<div class="col-lg-4 col-md-4">
								<div class="footer_widget">
									<h4 class="widget_title">Layouts</h4>
									<ul class="footer-menu">
										<li><a href="#">Home Page</a></li>
										<li><a href="#">About Page</a></li>
										<li><a href="#">Service Page</a></li>
										<li><a href="#">Property Page</a></li>
										<li><a href="#">Contact Page</a></li>
										<li><a href="#">Single Blog</a></li>
									</ul>
								</div>
							</div>

							<div class="col-lg-4 col-md-4">
								<div class="footer_widget">
									<h4 class="widget_title">All Sections</h4>
									<ul class="footer-menu">
										<li><a href="#">Headers<span class="new">New</span></a></li>
										<li><a href="#">Features</a></li>
										<li><a href="#">Attractive<span class="new">New</span></a></li>
										<li><a href="#">Testimonials</a></li>
										<li><a href="#">Videos</a></li>
										<li><a href="#">Footers</a></li>
									</ul>
								</div>
							</div>

							<div class="col-lg-4 col-md-4">
								<div class="footer_widget">
									<h4 class="widget_title">Company</h4>
									<ul class="footer-menu">
										<li><a href="#">About</a></li>
										<li><a href="#">Blog</a></li>
										<li><a href="#">Pricing</a></li>
										<li><a href="#">Affiliate</a></li>
										<li><a href="#">Login</a></li>
										<li><a href="#">Changelog<span class="update">Update</span></a></li>
									</ul>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer-bottom">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-12 col-md-12 text-center">
						<p class="mb-0">
							� 2021 SkillUp. Designd By <a href="https://themezhub.com">ThemezHub</a>.
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- ============================ Footer End ================================== -->

	<!-- Log In Modal -->
	<div class="modal fade" id="login" tabindex="-1" role="dialog"
		aria-labelledby="loginmodal" aria-hidden="true">
		<div class="modal-dialog modal-xl login-pop-form" role="document">
			<div class="modal-content overli" id="loginmodal">
				<div class="modal-header">
					<h5 class="modal-title">Login Your Account</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
					</button>
				</div>
				<div class="modal-body">
					<div class="login-form">
						<form>

							<div class="form-group">
								<label>User Name</label>
								<div class="input-with-icon">
									<input type="text" class="form-control"
										placeholder="User or email"> <i class="ti-user"></i>
								</div>
							</div>

							<div class="form-group">
								<label>Password</label>
								<div class="input-with-icon">
									<input type="password" class="form-control"
										placeholder="*******"> <i class="ti-unlock"></i>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-xl-4 col-lg-4 col-4">
									<input id="admin" class="checkbox-custom" name="admin"
										type="checkbox"> <label for="admin"
										class="checkbox-custom-label">Admin</label>
								</div>
								<div class="col-xl-4 col-lg-4 col-4">
									<input id="student" class="checkbox-custom" name="student"
										type="checkbox" checked> <label for="student"
										class="checkbox-custom-label">Student</label>
								</div>
								<div class="col-xl-4 col-lg-4 col-4">
									<input id="instructor" class="checkbox-custom"
										name="instructor" type="checkbox"> <label
										for="instructor" class="checkbox-custom-label">Tutors</label>
								</div>
							</div>

							<div class="form-group">
								<button type="submit"
									class="btn btn-md full-width theme-bg text-white">Login</button>
							</div>

							<div class="rcs_log_125 pt-2 pb-3">
								<span>Or Login with Social Info</span>
							</div>
							<div class="rcs_log_126 full">
								<ul class="social_log_45 row">
									<li class="col-xl-4 col-lg-4 col-md-4 col-4"><a
										href="javascript:void(0);" class="sl_btn"><i
											class="ti-facebook text-info"></i>Facebook</a></li>
									<li class="col-xl-4 col-lg-4 col-md-4 col-4"><a
										href="javascript:void(0);" class="sl_btn"><i
											class="ti-google text-danger"></i>Google</a></li>
									<li class="col-xl-4 col-lg-4 col-md-4 col-4"><a
										href="javascript:void(0);" class="sl_btn"><i
											class="ti-twitter theme-cl"></i>Twitter</a></li>
								</ul>
							</div>

						</form>
					</div>
				</div>
				<div class="crs_log__footer d-flex justify-content-between mt-0">
					<div class="fhg_45">
						<p class="musrt">
							Don't have account? <a href="signup.html" class="theme-cl">SignUp</a>
						</p>
					</div>
					<div class="fhg_45">
						<p class="musrt">
							<a href="forgot.html" class="text-danger">Forgot Password?</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Modal -->

	<a id="back2Top" class="top-scroll" title="Back to top" href="#"><i
		class="ti-arrow-up"></i></a>


	</div>
	<!-- ============================================================== -->
	<!-- End Wrapper -->
	<!-- ============================================================== -->

	<!-- ============================================================== -->
	<!-- All Jquery -->
	<!-- ============================================================== -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/select2.min.js"></script>
	<script src="assets/js/slick.js"></script>
	<script src="assets/js/moment.min.js"></script>
	<script src="assets/js/daterangepicker.js"></script>
	<script src="assets/js/summernote.min.js"></script>
	<script src="assets/js/metisMenu.min.js"></script>
	<script src="assets/js/custom.js"></script>
	<!-- ============================================================== -->
	<!-- This page plugins -->
	<!-- ============================================================== -->

</body>
</html>