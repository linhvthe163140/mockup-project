package fa.training.entity;

public class Book {
	private int bookID;
	private String bookTitle;
	private String author;
	private String brief;
	private String publisher;
	private String content;
	private Category category;
	private String imageURL;
	//new Comment
	
	public Book() {
		// TODO Auto-generated constructor stub
	}

	public Book(int bookID, String bookTitle, String author, String brief, String publisher, String content,
			Category category, String imageURL) {
		super();
		this.bookID = bookID;
		this.bookTitle = bookTitle;
		this.author = author;
		this.brief = brief;
		this.publisher = publisher;
		this.content = content;
		this.category = category;
		this.imageURL = imageURL;
	}

	public int getBookID() {
		return bookID;
	}

	public void setBookID(int bookID) {
		this.bookID = bookID;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	@Override
	public String toString() {
		return "Book [bookID=" + bookID + ", bookTitle=" + bookTitle + ", author=" + author + ", brief=" + brief
				+ ", publisher=" + publisher + ", content=" + content + ", category=" + category + ", imageURL="
				+ imageURL + "]";
	}
	
}
