<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">
<head>
<meta charset="utf-8" />
<meta name="author" content="Themezhub" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SkillUp - Online Learning Platform</title>

<!-- Custom CSS -->
<link href="assets/css/styles.css" rel="stylesheet">

</head>

<body>

	<div id="main-wrapper">
		<div class="header header-light">
			<div class="container">
				<%@include file="components/admin-header.jsp"%>
			</div>
		</div>
		<div class="clearfix"></div>
		<!-- ============================================================== -->
		<!-- Top header  -->
		<!-- ============================================================== -->

		<!-- ============================ Dashboard: Dashboard Start ================================== -->
		<section class="gray pt-4">
			<div class="container-fluid">

				<div class="row">

					<div class="col-lg-3 col-md-3">
						<%@include file="components/admin-sidebar.jsp"%>
					</div>

					<div class="col-lg-9 col-md-9 col-sm-12">

						<!-- Row -->
						<div class="row justify-content-between">
							<div class="col-lg-12 col-md-12 col-sm-12 pb-4">
								<div
									class="dashboard_wrap d-flex align-items-center justify-content-between">
									<div class="arion">
										<nav class="transparent">
											<ol class="breadcrumb">
												<li class="breadcrumb-item"><a href="#">Home</a></li>
												<li class="breadcrumb-item active" aria-current="page">Add
													New Course</li>
											</ol>
										</nav>
									</div>
								</div>
							</div>
						</div>
						<!-- /Row -->

						<div class="row">
							<div class="col-xl-12 col-lg-12 col-md-12">
								<div class="dashboard_wrap">

									<div class="form_blocs_wrap">
										<form>
											<div class="row justify-content-between">

												<div class="col-xl-3 col-lg-4 col-md-5 col-sm-12">
													<div class="nav flex-column nav-pills me-3"
														id="v-pills-tab" role="tablist"
														aria-orientation="vertical">
														<button class="nav-link active" id="v-pills-basic-tab"
															data-toggle="pill" data-target="#v-pills-basic"
															type="button" role="tab" aria-controls="v-pills-basic"
															aria-selected="true">Basic</button>
														<button class="nav-link" id="v-pills-requirements-tab"
															data-toggle="pill" data-target="#v-pills-requirements"
															type="button" role="tab"
															aria-controls="v-pills-requirements"
															aria-selected="false">Requirements</button>
														<button class="nav-link" id="v-pills-pricing-tab"
															data-toggle="pill" data-target="#v-pills-pricing"
															type="button" role="tab" aria-controls="v-pills-pricing"
															aria-selected="false">Pricing</button>
														<button class="nav-link" id="v-pills-media-tab"
															data-toggle="pill" data-target="#v-pills-media"
															type="button" role="tab" aria-controls="v-pills-media"
															aria-selected="false">Media</button>
														<button class="nav-link" id="v-pills-seo-tab"
															data-toggle="pill" data-target="#v-pills-seo"
															type="button" role="tab" aria-controls="v-pills-seo"
															aria-selected="false">SEO</button>
														<button class="nav-link" id="v-pills-finish-tab"
															data-toggle="pill" data-target="#v-pills-finish"
															type="button" role="tab" aria-controls="v-pills-finish"
															aria-selected="false">Finish</button>
													</div>
												</div>

												<div class="col-xl-9 col-lg-8 col-md-7 col-sm-12">

													<div class="tab-content" id="v-pills-tabContent">
														<!-- Basic -->
														<div class="tab-pane fade show active" id="v-pills-basic"
															role="tabpanel" aria-labelledby="v-pills-basic-tab">

															<div class="form-group smalls">
																<label>Course Title*</label> <input type="text"
																	class="form-control" placeholder="Enter Course Title">
															</div>

															<div class="form-group smalls">
																<label>Short Description</label> <input type="text"
																	class="form-control">
															</div>

															<div class="form-group smalls">
																<label>Description</label>
																<textarea class="summernote"></textarea>
															</div>

															<div class="form-group smalls">
																<label>Category*</label>
																<div class="simple-input">
																	<select id="cates" class="form-control">
																		<option value="">&nbsp;</option>
																		<option value="1">Parent</option>
																		<option value="2">Banking</option>
																		<option value="3">Medical</option>
																		<option value="4">Insurence</option>
																		<option value="5">Finance & Accounting</option>
																	</select>
																</div>
															</div>

															<div class="form-group smalls">
																<label>Level</label>
																<div class="simple-input">
																	<select id="lvl" class="form-control">
																		<option value="">&nbsp;</option>
																		<option value="1">Beginner</option>
																		<option value="2">Basic</option>
																		<option value="3">Mediam</option>
																		<option value="4">Advance</option>
																	</select>
																</div>
															</div>

															<div class="form-group smalls">
																<label>Language</label>
																<div class="simple-input">
																	<select id="lgu" class="form-control">
																		<option value="">&nbsp;</option>
																		<option value="1">Spanish</option>
																		<option value="2">French</option>
																		<option value="3">English</option>
																		<option value="4">Other</option>
																	</select>
																</div>
															</div>

															<div class="form-group smalls">
																<input id="l2l" class="checkbox-custom" name="l2l"
																	type="checkbox"> <label for="l2l"
																	class="checkbox-custom-label">Check this for
																	featured course</label>
															</div>

															<div
																class="d-flex align-items-center justify-content-center">
																<ul class="alios_slide_nav">
																	<li><a href="#" class="btn btn_slide disabled"><i
																			class="fas fa-arrow-left"></i></a></li>
																	<li><a href="#" class="btn btn_slide"><i
																			class="fas fa-arrow-right"></i></a></li>
																</ul>
															</div>

														</div>

														<!-- requirements -->
														<div class="tab-pane fade" id="v-pills-requirements"
															role="tabpanel"
															aria-labelledby="v-pills-requirements-tab">
															<div class="form-group smalls">
																<label>Requirements</label>
																<div class="row m-0">
																	<input type="text" class="col-9 col-sm-9 form-control"
																		placeholder="Provide Requirements">
																	<div class="col-3 col-sm-3">
																		<button class="btn theme-bg" type="button">
																			<i class="fas fa-plus"></i>
																		</button>
																	</div>
																</div>
															</div>

															<div class="form-group smalls">
																<ul class="lists-4">
																	<li>At vero eos et accusamus et iusto odio
																		dignissimos ducimus</li>
																	<li>At vero eos et accusamus et iusto odio
																		dignissimos ducimus</li>
																	<li>At vero eos et accusamus et iusto odio
																		dignissimos ducimus</li>
																	<li>At vero eos et accusamus et iusto odio
																		dignissimos ducimus</li>
																	<li>At vero eos et accusamus et iusto odio
																		dignissimos ducimus</li>
																</ul>
															</div>

															<div
																class="d-flex align-items-center justify-content-center">
																<ul class="alios_slide_nav">
																	<li><a href="#" class="btn btn_slide"><i
																			class="fas fa-arrow-left"></i></a></li>
																	<li><a href="#" class="btn btn_slide"><i
																			class="fas fa-arrow-right"></i></a></li>
																</ul>
															</div>
														</div>

														<!-- pricing -->
														<div class="tab-pane fade" id="v-pills-pricing"
															role="tabpanel" aria-labelledby="v-pills-pricing-tab">

															<div class="form-group smalls">
																<div class="drios">
																	<input id="l23" class="checkbox-custom" name="l23"
																		type="checkbox"> <label for="l23"
																		class="checkbox-custom-label">Check this if
																		Course id Free</label>
																</div>
															</div>

															<div class="form-group smalls">
																<label>Course Price($)</label> <input type="text"
																	class="form-control" placeholder="Enter Course Price">
															</div>

															<div class="form-group smalls">
																<label>Discount Price($)</label> <input type="text"
																	class="form-control" placeholder="Enter Discount Price">
																<div class="drios">
																	<input id="l22" class="checkbox-custom" name="l22"
																		type="checkbox"> <label for="l22"
																		class="checkbox-custom-label">Enable This
																		Discount</label>
																</div>
															</div>

															<div
																class="d-flex align-items-center justify-content-center">
																<ul class="alios_slide_nav">
																	<li><a href="#" class="btn btn_slide"><i
																			class="fas fa-arrow-left"></i></a></li>
																	<li><a href="#" class="btn btn_slide"><i
																			class="fas fa-arrow-right"></i></a></li>
																</ul>
															</div>
														</div>

														<!-- media -->
														<div class="tab-pane fade" id="v-pills-media"
															role="tabpanel" aria-labelledby="v-pills-media-tab">
															<div class="form-group smalls">
																<label>Course Provider</label>
																<div class="simple-input">
																	<select id="vid" class="form-control">
																		<option value="">&nbsp;</option>
																		<option value="1">YouTube</option>
																		<option value="2">Vivo</option>
																		<option value="3">Facebook</option>
																		<option value="4">Other</option>
																	</select>
																</div>
															</div>

															<div class="form-group smalls">
																<label>Video URL</label> <input type="text"
																	class="form-control"
																	placeholder="https://www.youtube.com/watch?v=ExXhmuH-cw8">
															</div>

															<div class="form-group smalls">
																<label>Thumbnail</label>
																<div class="custom-file">
																	<input type="file" class="custom-file-input"
																		id="customFile"> <label
																		class="custom-file-label" for="customFile">Choose
																		file</label>
																</div>
															</div>

															<div
																class="d-flex align-items-center justify-content-center">
																<ul class="alios_slide_nav">
																	<li><a href="#" class="btn btn_slide"><i
																			class="fas fa-arrow-left"></i></a></li>
																	<li><a href="#" class="btn btn_slide"><i
																			class="fas fa-arrow-right"></i></a></li>
																</ul>
															</div>

														</div>

														<!-- seo -->
														<div class="tab-pane fade" id="v-pills-seo"
															role="tabpanel" aria-labelledby="v-pills-seo-tab">
															<div class="form-group smalls">
																<label>Course SEO Title</label> <input type="text"
																	class="form-control" placeholder="Enter Course Title">
															</div>

															<div class="form-group smalls">
																<label>Course SEO Description</label>
																<textarea class="form-control"
																	placeholder="Enter Course Description"></textarea>
															</div>

															<div
																class="d-flex align-items-center justify-content-center">
																<ul class="alios_slide_nav">
																	<li><a href="#" class="btn btn_slide"><i
																			class="fas fa-arrow-left"></i></a></li>
																	<li><a href="#" class="btn btn_slide"><i
																			class="fas fa-arrow-right"></i></a></li>
																</ul>
															</div>

														</div>

														<!-- finish -->
														<div class="tab-pane fade" id="v-pills-finish"
															role="tabpanel" aria-labelledby="v-pills-finish-tab">
															<div class="succ_wrap">
																<div class="succ_121">
																	<i class="fas fa-thumbs-up"></i>
																</div>
																<div class="succ_122">
																	<h4>Course Successfully Added</h4>
																	<p>Lorem ipsum dolor sit amet, consectetur
																		adipiscing elit, sed do eiusmod tempor incididunt ut
																		labore et dolore magna aliqua.</p>
																</div>
																<div class="succ_123">
																	<a href="course-detail.html"
																		class="btn theme-bg text-white">View Course</a>
																</div>
															</div>
														</div>
													</div>
												</div>

											</div>
										</form>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
		<!-- ============================ Dashboard: Dashboard End ================================== -->

		<!-- ============================ Call To Action ================================== -->
		<section class="theme-bg call_action_wrap-wrap">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">

						<div class="call_action_wrap">
							<div class="call_action_wrap-head">
								<h3>Do You Have Questions ?</h3>
								<span>We'll help you to grow your career and growth.</span>
							</div>
							<a href="#" class="btn btn-call_action_wrap">Contact Us Today</a>
						</div>

					</div>
				</div>
			</div>
		</section>
		<!-- ============================ Call To Action End ================================== -->

		<!-- ============================ Footer Start ================================== -->
		<footer class="dark-footer skin-dark-footer style-2">
			<div class="footer-middle">
				<div class="container">
					<div class="row">

						<div class="col-lg-5 col-md-5">
							<div class="footer_widget">
								<img src="assets/img/logo-light.png"
									class="img-footer small mb-2" alt="" />
								<h4 class="extream mb-3">
									Do you need help with<br>anything?
								</h4>
								<p>Receive updates, hot deals, tutorials, discounts sent
									straignt in your inbox every month</p>
								<div class="foot-news-last">
									<div class="input-group">
										<input type="text" class="form-control"
											placeholder="Email Address">
										<div class="input-group-append">
											<button type="button"
												class="input-group-text theme-bg b-0 text-light">Subscribe</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-6 col-md-7 ml-auto">
							<div class="row">

								<div class="col-lg-4 col-md-4">
									<div class="footer_widget">
										<h4 class="widget_title">Layouts</h4>
										<ul class="footer-menu">
											<li><a href="#">Home Page</a></li>
											<li><a href="#">About Page</a></li>
											<li><a href="#">Service Page</a></li>
											<li><a href="#">Property Page</a></li>
											<li><a href="#">Contact Page</a></li>
											<li><a href="#">Single Blog</a></li>
										</ul>
									</div>
								</div>

								<div class="col-lg-4 col-md-4">
									<div class="footer_widget">
										<h4 class="widget_title">All Sections</h4>
										<ul class="footer-menu">
											<li><a href="#">Headers<span class="new">New</span></a></li>
											<li><a href="#">Features</a></li>
											<li><a href="#">Attractive<span class="new">New</span></a></li>
											<li><a href="#">Testimonials</a></li>
											<li><a href="#">Videos</a></li>
											<li><a href="#">Footers</a></li>
										</ul>
									</div>
								</div>

								<div class="col-lg-4 col-md-4">
									<div class="footer_widget">
										<h4 class="widget_title">Company</h4>
										<ul class="footer-menu">
											<li><a href="#">About</a></li>
											<li><a href="#">Blog</a></li>
											<li><a href="#">Pricing</a></li>
											<li><a href="#">Affiliate</a></li>
											<li><a href="#">Login</a></li>
											<li><a href="#">Changelog<span class="update">Update</span></a></li>
										</ul>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="footer-bottom">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-12 col-md-12 text-center">
							<p class="mb-0">
								� 2021 SkillUp. Designd By <a href="https://themezhub.com">ThemezHub</a>.
							</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- ============================ Footer End ================================== -->

		<!-- Log In Modal -->
		<div class="modal fade" id="login" tabindex="-1" role="dialog"
			aria-labelledby="loginmodal" aria-hidden="true">
			<div class="modal-dialog modal-xl login-pop-form" role="document">
				<div class="modal-content overli" id="loginmodal">
					<div class="modal-header">
						<h5 class="modal-title">Login Your Account</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<div class="login-form">
							<form>

								<div class="form-group">
									<label>User Name</label>
									<div class="input-with-icon">
										<input type="text" class="form-control"
											placeholder="User or email"> <i class="ti-user"></i>
									</div>
								</div>

								<div class="form-group">
									<label>Password</label>
									<div class="input-with-icon">
										<input type="password" class="form-control"
											placeholder="*******"> <i class="ti-unlock"></i>
									</div>
								</div>

								<div class="form-group row">
									<div class="col-xl-4 col-lg-4 col-4">
										<input id="admin" class="checkbox-custom" name="admin"
											type="checkbox"> <label for="admin"
											class="checkbox-custom-label">Admin</label>
									</div>
									<div class="col-xl-4 col-lg-4 col-4">
										<input id="student" class="checkbox-custom" name="student"
											type="checkbox" checked> <label for="student"
											class="checkbox-custom-label">Student</label>
									</div>
									<div class="col-xl-4 col-lg-4 col-4">
										<input id="instructor" class="checkbox-custom"
											name="instructor" type="checkbox"> <label
											for="instructor" class="checkbox-custom-label">Tutors</label>
									</div>
								</div>

								<div class="form-group">
									<button type="submit"
										class="btn btn-md full-width theme-bg text-white">Login</button>
								</div>

								<div class="rcs_log_125 pt-2 pb-3">
									<span>Or Login with Social Info</span>
								</div>
								<div class="rcs_log_126 full">
									<ul class="social_log_45 row">
										<li class="col-xl-4 col-lg-4 col-md-4 col-4"><a
											href="javascript:void(0);" class="sl_btn"><i
												class="ti-facebook text-info"></i>Facebook</a></li>
										<li class="col-xl-4 col-lg-4 col-md-4 col-4"><a
											href="javascript:void(0);" class="sl_btn"><i
												class="ti-google text-danger"></i>Google</a></li>
										<li class="col-xl-4 col-lg-4 col-md-4 col-4"><a
											href="javascript:void(0);" class="sl_btn"><i
												class="ti-twitter theme-cl"></i>Twitter</a></li>
									</ul>
								</div>

							</form>
						</div>
					</div>
					<div class="crs_log__footer d-flex justify-content-between mt-0">
						<div class="fhg_45">
							<p class="musrt">
								Don't have account? <a href="signup.html" class="theme-cl">SignUp</a>
							</p>
						</div>
						<div class="fhg_45">
							<p class="musrt">
								<a href="forgot.html" class="text-danger">Forgot Password?</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Modal -->

		<a id="back2Top" class="top-scroll" title="Back to top" href="#"><i
			class="ti-arrow-up"></i></a>


	</div>
	<!-- ============================================================== -->
	<!-- End Wrapper -->
	<!-- ============================================================== -->

	<!-- ============================================================== -->
	<!-- All Jquery -->
	<!-- ============================================================== -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/select2.min.js"></script>
	<script src="assets/js/slick.js"></script>
	<script src="assets/js/moment.min.js"></script>
	<script src="assets/js/daterangepicker.js"></script>
	<script src="assets/js/summernote.min.js"></script>
	<script src="assets/js/metisMenu.min.js"></script>
	<script src="assets/js/custom.js"></script>

	<!-- ============================================================== -->
	<!-- This page plugins -->
	<!-- ============================================================== -->

</body>
</html>