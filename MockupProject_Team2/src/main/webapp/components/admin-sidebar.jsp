<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<div class="dashboard-navbar">
	<div class="d-user-avater">
		<img src="https://via.placeholder.com/500x500"
			class="img-fluid avater" alt="">
		<h4>Adam Harshvardhan</h4>
		<span>Admin</span>
	</div>

	<div class="d-navigation">
		<ul id="side-menu">
			<li class="active"><a href="dashboard.html"><i
					class="fas fa-th"></i>Dashboard</a></li>
			<li class="dropdown"><a href="javascript:void(0);"><i
					class="fas fa-book"></i>Books<span class="ti-angle-left"></span></a>
				<ul class="nav nav-second-level">
					<li><a href="book-list.html">Manage Books</a></li>
					<li><a href="add-new-book.html">Add New Books</a></li>
				</ul></li>
			<li class="dropdown"><a href="javascript:void(0);"><i
					class="fas fa-user"></i>Users<span class="ti-angle-left"></span></a>
				<ul class="nav nav-second-level">
					<li><a href="manage-students.html">Manage Users</a></li>
					<li><a href="add-students.html">Add New User</a></li>
				</ul></li>
		</ul>
	</div>

</div>

<!-- ============================ Dashboard: Dashboard End ================================== -->

<!-- ============================ Call To Action End ================================== -->

<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/select2.min.js"></script>
<script src="assets/js/slick.js"></script>
<script src="assets/js/moment.min.js"></script>
<script src="assets/js/daterangepicker.js"></script>
<script src="assets/js/summernote.min.js"></script>
<script src="assets/js/metisMenu.min.js"></script>
<script src="assets/js/custom.js"></script>

<!-- Morris.js charts -->
<script src="assets/js/raphael.min.js"></script>
<script src="assets/js/morris.min.js"></script>

<!-- Custom Morrisjs JavaScript -->
<script src="assets/js/morris.js"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->

</body>
</html>