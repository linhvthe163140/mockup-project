<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="zxx">
	<head>
		<meta charset="utf-8" />
		<meta name="author" content="Themezhub" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SkillUp - Online Learning Platform</title>
		 
        <!-- Custom CSS -->
        <link href="assets/css/styles.css" rel="stylesheet">
		
    </head>
	
    <body>
        <div id="main-wrapper">
			<div class="header header-light">
				<div class="container">
					<%@include file="components/admin-header.jsp"%>
				</div>
			</div>
			<!-- End Navigation -->
			<div class="clearfix"></div>
			<section class="gray pt-4">
				<div class="container-fluid">
										
					<div class="row">
					
						<div class="col-lg-3 col-md-3">
							<%@include file="components/admin-sidebar.jsp"%>
						</div>	
						
						<div class="col-lg-9 col-md-9 col-sm-12">
							
							<!-- Row -->
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 pb-4">
									<nav aria-label="breadcrumb">
										<ol class="breadcrumb">
											<li class="breadcrumb-item"><a href="#">Home</a></li>
											<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
										</ol>
									</nav>
								</div>
							</div>
							
							<!-- Row -->
							<div class="row">
						
								<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
									<div class="dashboard_stats_wrap">
										<div class="rounded-circle p-4 p-sm-4 d-inline-flex align-items-center justify-content-center theme-bg mb-2"><div class="position-absolute text-white h5 mb-0"><i class="fas fa-book"></i></div></div>
										<div class="dashboard_stats_wrap_content"><a href="book-list.html">Books List</a></div>
									</div>	
								</div>
								<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
									<div class="dashboard_stats_wrap">
										<div class="rounded-circle p-4 p-sm-4 d-inline-flex align-items-center justify-content-center theme-bg mb-2"><div class="position-absolute text-white h5 mb-0"><i class="fas fa-book"></i></div></div>
										<div class="dashboard_stats_wrap_content"><a href="user-list.html">Users List</a></div>
									</div>	
								</div>
							</div>
							<!-- /Row -->
							
						</div>
					
					</div>
					
				</div>
			</section>
			
			

		</div>

		<!-- ============================================================== -->
		<!-- All Jquery -->
		<!-- ============================================================== -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/select2.min.js"></script>
		<script src="assets/js/slick.js"></script>
		<script src="assets/js/moment.min.js"></script>
		<script src="assets/js/daterangepicker.js"></script> 
		<script src="assets/js/summernote.min.js"></script>
		<script src="assets/js/metisMenu.min.js"></script>	
		<script src="assets/js/custom.js"></script>
		
		<!-- Morris.js charts -->
		<script src="assets/js/raphael.min.js"></script>
		<script src="assets/js/morris.min.js"></script>
		
		<!-- Custom Morrisjs JavaScript -->
		<script src="assets/js/morris.js"></script>
		<!-- ============================================================== -->
		<!-- This page plugins -->
		<!-- ============================================================== -->		

	</body>
</html>