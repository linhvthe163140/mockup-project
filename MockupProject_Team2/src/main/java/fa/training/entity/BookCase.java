package fa.training.entity;

import java.util.ArrayList;

public class BookCase {
	private int bookCaseID;
	private String bookCaseName;
	private ArrayList<Book> listBooks;
	
	public BookCase() {
		// TODO Auto-generated constructor stub
	}

	public BookCase(int bookCaseID, String bookCaseName, ArrayList<Book> listBooks) {
		super();
		this.bookCaseID = bookCaseID;
		this.bookCaseName = bookCaseName;
		this.listBooks = listBooks;
	}

	public int getBookCaseID() {
		return bookCaseID;
	}

	public void setBookCaseID(int bookCaseID) {
		this.bookCaseID = bookCaseID;
	}

	public String getBookCaseName() {
		return bookCaseName;
	}

	public void setBookCaseName(String bookCaseName) {
		this.bookCaseName = bookCaseName;
	}

	public ArrayList<Book> getListBooks() {
		return listBooks;
	}

	public void setListBooks(ArrayList<Book> listBooks) {
		this.listBooks = listBooks;
	}

	@Override
	public String toString() {
		return "BookCase [bookCaseID=" + bookCaseID + ", bookCaseName=" + bookCaseName + ", listBooks=" + listBooks
				+ "]";
	}
	
}
